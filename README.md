# Prova Prática EGS 2

## Introdução
O projeto foi criado e desenvolvido durante a prova de Engenharia de Software 2, na Faculdade de Técnologia Senac Pelotas.

## Clone
``` bash
#via https
git clone https://gitlab.com/thaua97/TesteES.git

#via ssh
git clone git@gitlab.com:thaua97/TesteES.git

```
